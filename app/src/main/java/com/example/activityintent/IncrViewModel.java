package com.example.activityintent;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class IncrViewModel extends ViewModel {

    private MutableLiveData<Integer> count;

    // instanciation si il count n'existe pas
    public MutableLiveData<Integer> getCount(){
        if(count==null){
            count = new MutableLiveData<Integer>();
        }
        return count;
    }
}
