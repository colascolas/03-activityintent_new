package com.example.activityintent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButton;
    private TextView mTextView;
    private int partialCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        mButton = findViewById(R.id.buttonincr);
        mTextView = findViewById(R.id.partialcount);
        if(savedInstanceState!=null){
            partialCount = savedInstanceState.getInt("partial");
        }else {
            partialCount = getIntent().getIntExtra("compteur", 0);
        }
        mTextView.setText(String.valueOf(partialCount));
        /* prise en charge du callback */
        mButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        partialCount++;
        mTextView.setText(String.valueOf(partialCount));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("partialcount",partialCount);
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        Log.i("LIFE","OnPause Activité 2");
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LIFE", "OnResume Activité 2");
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("LIFE", "OnStart Activité 2");
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LIFE", "OnStop Activité 2");
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("partial",partialCount);
    }
}