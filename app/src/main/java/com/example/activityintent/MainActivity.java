package com.example.activityintent;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class MainActivity extends AppCompatActivity {

    private Button mButton;
    private TextView mTextView;
    private EditText mEdit;

    private IncrViewModel mVm;

    @Override
    protected void onStart() {
        Log.i("LIFE","OnStart Activité 1");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.i("LIFE","OnStop Activité 1");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i("LIFE","OnDestroy Activité 1");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.i("LIFE","OnPause Activité 1");
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LIFE","OnResume Activité 1");

    }

    /**
     * Ici, savedInstanceState n'est pas utilisé car l'état est conservé dans le ViewModel
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = findViewById(R.id.buttonB);
        mTextView = findViewById(R.id.count);

        // récupération (ou instanciation) du ViewModel lié à l'activité
        mVm = new ViewModelProvider(this).get(IncrViewModel.class);

        // création d'un observateur
        Observer<Integer> mObs = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mTextView.setText(String.valueOf(integer));
            }
        };

        // placement de l'observateur du mVm. S'il est modifié ou s'il est recréé dans le cadre
        // d'un changement de configuration, il sera automatiquement mis à jour.
        mVm.getCount().observe(this,mObs);

        // instanciation du launcher en spécifiant le type de contrat (ici Intent in, Intent out) ainsi
        // que le callback à appeler lors du retour.
        final ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if(result.getResultCode()==RESULT_OK) {
                            mVm.getCount().setValue(result.getData().getIntExtra("partialcount", 0));
                        }
                    }
                }
        );



        /* installation du listener de bouton : ici instanciation anonyme */
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("compteur",mVm.getCount().getValue());
                activityResultLauncher.launch(intent);

            }
        });
        ActivityResultLauncher<Intent> resultLauncher;

        Toast.makeText(this,"Sortie de OnCreate",Toast.LENGTH_LONG).show();
    }

}